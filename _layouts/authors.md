---
layout: default
---

<h1>{{ page.tag }}</h1>
<ul class="list-reset">
{% assign sorted_texts = site.pages | sort: 'year' | reverse %}
{% for text in sorted_texts %}
    {% if text.author == page.tag %}
    <li>
        <div class="clearfix">
            <div class="col col-2 right-align pr1">
                <span>{{ text.year }}</span>
            </div>
            <div class="col col-10 left-align pl1">
            {% if text.draft == true %}
                {{ text.title }} （近日公開）
            {% else %}
                <a href="{{text.url | relative_url }}">
                    {{ text.title }}
                </a>
            {% endif %}
            </div>
        </div>
    </li>
    {% endif %}
{% endfor %}
</ul>