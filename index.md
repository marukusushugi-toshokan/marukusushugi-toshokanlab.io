---
layout: default
---

{% assign sorted_authors = site.authors | sort: 'tag' | reverse %}
{% for author in sorted_authors %}
## {{ author.tag }}
<ul class="list-reset">
{% assign sorted_pages = site.pages | sort: 'year' | reverse %}
{% for page in sorted_pages %}
    {% if page.author == author.tag %}
    <li>
        <div class="clearfix">
            <div class="col col-2 right-align pr1">
                <span>{{ page.year }}</span>
            </div>
            <div class="col col-10 left-align pl1">
            {% if page.draft == true %}
                {{ page.title }} （近日公開）
            {% else %}
                <a href="{{page.url | relative_url }}">
                    {{ page.title }}
                </a>
            {% endif %}
            </div>
        </div>
    </li>
    {% endif %}
{% endfor %}
</ul>
{% endfor %}
